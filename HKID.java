

import java.util.Random;

public class HKID {

	public static void main(String[] args) {

		System.err.println(validate("S123456(7)"));
		System.err.println(genRandomHKID());

	}

	public static boolean validate(String hkid) {
		hkid = hkid.replaceAll(" ", "").toUpperCase();

		String prefixU = "";
		String serial = "";
		String checkdigit = "";

		if ((int) hkid.charAt(1) >= 65) {
			prefixU = hkid.substring(0, 2);
			serial = hkid.substring(2, 8);
		} else {
			prefixU = hkid.substring(0, 1);
			serial = hkid.substring(1, 7);
		}
		checkdigit = hkid.substring(hkid.length() - 2, hkid.length() - 1);
		System.err.println("prefixU   " + prefixU);
		System.err.println("serial   " + serial);
		System.err.println("checkdigit   " + checkdigit);

		long value = 0;
		if (prefixU.length() == 2) {
			value += (prefixU.charAt(0) - 55) * 9 + (prefixU.charAt(1) - 55) * 8;
		} else if (prefixU.length() == 1) {
			value += 36 * 9 + (prefixU.charAt(0) - 55) * 8;
		}
		for (int i = 0; i < 6; i++) {
			value += Integer.parseInt(serial.substring(i, i + 1)) * (7 - i);
		}
		long reminder = value % 11;
		long valid_checkdigit = 11 - reminder;
		try {
			if (valid_checkdigit == 10) {
				return checkdigit.equals("A");
			}
			if (valid_checkdigit == 11) {
				return checkdigit.equals("0");
			}
			if (valid_checkdigit == Integer.parseInt(checkdigit)) {
				return true;
			}
		} catch (Exception e) {
		}
		return false;
	}

	private static String genRandomHKID() {
		String checkdigit = "";
		String prefix = (char) (new Random().nextInt(25) + 65) + "";
		String serial = (int) ((Math.random() * 9 + 1) * 100000) + "";
		long value = 0;
		value += 58 * 9 + (prefix.charAt(0) - 55) * 8;
		for (int i = 0; i < 6; i++) {
			value += Integer.parseInt(serial.substring(i, i + 1)) * (7 - i);
		}
		long reminder = value % 11;
		long valid_checkdigit = 11 - reminder;
		checkdigit = valid_checkdigit + "";

		if (valid_checkdigit == 10)
			checkdigit = "A";  
		if (valid_checkdigit == 11)
			checkdigit = "0";

		return prefix + serial + "(" + checkdigit + ")";

	}

}